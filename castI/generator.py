## Local functions
def copy_to_file(fw, from_file_name, replace_string):
    "Copies file_from_name to file fw, while replacing ___ with replace string"
    fr = open( from_file_name, "r" )
    for line in fr:
        fw.write(line.replace("___", replace_string))
    fr.close 

def generate_assigment(fw, problemIDs ):
    "Generates a single assigment for the given IDs"

    for i in range(len(problemIDs)):
        fw.write("%" * 80 + "\n")
        copy_to_file(fw, str("src/problem"+ str(i+1) + ".tex"), problemIDs[i])

    return

## Main part of the file
semesterID = "_LS2020-21"
examID = [ "21.5.2021", "28.5.2021", "4.6.2021", "11.6.2021", "18.6.2021", "25.6.2021", "ab.09.2021" ]  
num_assignments = len(examID) # number of assigments to generate

# Generate variants 
import random
random.seed

problems = []

# Assignment 1
variants = list("AB") # A: simply-supported B: cantilever beams
selection = []

for i in range(num_assignments):
    structure = random.choice(variants)
    if structure == 'A':
        structure += str( random.randint(1,6) )
    else:
        structure += str( random.randint(1,16) )
    selection.append(structure)

problems.append(selection[:])

# Assignment 2-9
variants = list("ABCDEFGH")
for i in range(8):
    selection = random.sample( variants, num_assignments )
    problems.append( selection[:] )

# Generate individual assignments
problems = list(zip(*problems))
for i in range(num_assignments):
    fw = open(semesterID + "_zkouska" + str(i+1) + ".tex", "w") 
    
    copy_to_file(fw, "src/doc_start.tex", examID[i])   
    generate_assigment(fw, problems[i] )
    copy_to_file(fw, "src/doc_end.tex", "")

    fw.close
    
## Create new Makefile -- not completed yet
print_pages = ""

for i in range(1, num_assignments+1):
    print_pages += " " + str(i) + " " + str(i)
    
fw = open( "Makefile", "w")

copy_to_file(fw, "src/Makefile", print_pages)

fw.close
