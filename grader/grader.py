# -*- coding: utf-8 -*-
#------------------------------------------------------------------------------
import ezodf # to process .ods files
import numpy as np # to process test results
import smtplib, ssl # to send emails
from email.mime.text import MIMEText # needed to process UTF8 strings
from email.mime.multipart import MIMEMultipart # needed to process UTF8 strings
import time # to pause sending emails for a while
#------------------------------------------------------------------------------
doc = ezodf.opendoc("LS2019_2020_SMA1.ods")
exam_date = "15.9.2020"
exam_URL = "https://owncloud.cesnet.cz/index.php/s/a8WD6VFS4MAUiXZ"
number_of_students = 2
send_emails = True
#------------------------------------------------------------------------------
# Email data setup
port = 587  # For SSL
smtp_server = "mail.cvut.cz"
sender_email = "Jan.Zeman@cvut.cz"
sender_username = "zemanjan"
if send_emails:
    password = input("Type your password and press enter: ")
#------------------------------------------------------------------------------
sheet = doc.sheets[1] # Always process the second sheet

assert exam_date == sheet.name # Check whether we are processing the correct sheet

for row in range(2, 2+number_of_students):
    name, surname = sheet[row, 0].value, sheet[row, 1].value 
    receiver_email = sheet[row, 2].value

    message = "Dobrý den,\n\ndo systému KOS byly zadány následující výsledky Vaší zkoušky.\n"

    ## Process data for part II
    part_I = []
    passed_part_I = False

    message = message + "\nPrvní část\n"

    for col in range(3, 12):
        message = message + "  * příklad " + str(col-2) + ": "
        points = sheet[row, col].value

        if points == None:
            message = message + "nehodnoceno\n"
            points = 0
        else:
            message = message + str(points) + " b.\n"
        
        part_I = np.append(part_I, points) 

    passed_part_I = np.sum(part_I) >= 20 and (np.count_nonzero(part_I > 0) > 7)

    message = message + "Součet: " + str( np.sum(part_I) ) + " b. "

    if passed_part_I:
        message = message + "✔\n"
    else:
        message = message + "✘\n"
    
    ## Process data for part II
    part_II = []
    passed_part_II = False

    if passed_part_I: 
        message = message + ("\nDruhá část\n")
        
        for col in range(13, 16):
            message = message + "  * příklad " + str(col-12) + ": "
            points = sheet[row, col].value

            if points == None:
                message = message + "nehodnoceno\n"
                points = 0
            else:
                message = message + str(points) + " b.\n"

            part_II = np.append(part_II, points)
    
        # Check if passed part_II
        passed_part_II = np.sum(part_II) >= 20 and np.count_nonzero(part_II > 0) == 3

        message = message + "Součet: " + str( np.sum(part_II) ) + " b. "

        if passed_part_II:
            message = message + "✔\n"
        else:
            message = message + "✘\n"

    # Final evaluation of whole exam
    grade = "F" 

    if passed_part_I and passed_part_II:
        total = np.sum( part_I ) + np.sum( part_II )
        message = message + ("\nCelkový součet: " + str(total) + " b.\n")

        if total >= 72.0:
            grade = "A"
        elif total >= 64.0:
            grade = "B"
        elif total >= 56.0:
            grade = "C"
        elif total >= 48.0:
            grade = "D"
        elif total >= 40.0:
            grade = "E"
    
    message = message + "\nCelkové hodnocení zkoušky: " + grade + "\n"

    if grade == 'F':
        message = message + "\nZkoušku se Vám dnes bohužel nepodařilo složit"
        if passed_part_I and np.sum(part_I) >= 30:
            message = message + ", výsledek první části ale můžete využít při opravném termínu.\n"
        else:
            message = message + ".\n"
    else: 
        message = message + "\nBlahopřejeme k úspěšnému složení zkoušky.\n"
        message = message + "\nNezapomeňte prosím vyplnit krátký dotazník na https://bit.ly/3gkp8On a oficiální anketu na https://anketa.is.cvut.cz/html/anketa.\n"
    
    message = message + "\nJménem všech vyučujících předmětu SMA1 zdraví,\n\nJan Zeman\n" 

    message = message + "\nPS:\n"
    
    message = message + "  * řešení je dostupné na " + exam_URL + "\n"
    message = message + "  * bodování jednotlivých příkladů je dostupné na https://bit.ly/2X2JVgB (Sekce 2 a 3)\n"
    message = message + "  * pokud nesouhlasíte s hodnocením zkoušky, postupujte prosím dle instrukcí na https://bit.ly/2X2JVgB (Sekce 1)"

    ## Output to screen
    print(80*"-")
    print(name + ' ' + surname)
    print(80*"-")
    print(message)

    ## Send e-mail
    if send_emails:

        # Wait for one minute not to be disconnected from SMTP server
        if (row % 4 == 0):       
            print ( "\n> Pausing for one minute ... ")
            time.sleep (60)

        msg = MIMEMultipart("text")

        msg["Subject"] = "[SMA1] Výsledek zkoušky " + exam_date
        msg["From"] = sender_email
        msg["To"] = receiver_email

        msg.attach(MIMEText(message, "plain"))

        context = ssl.create_default_context()
        
        with smtplib.SMTP(smtp_server, port) as server:
            server.starttls(context=context)
            server.login(sender_username, password)
            server.sendmail(sender_email, receiver_email, msg.as_string())